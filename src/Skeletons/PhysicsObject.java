package Skeletons;

public class PhysicsObject {

	Vector pos, vel, force;
	double mass;
	
	public PhysicsObject(Vector pos, Vector vel, double mass) {
		this.pos = pos;
		this.vel = vel;
		this.mass = mass;
		this.force = new Vector(0, 0, 0);
	}
	
	public void setCollider(/* Empty for now */) {
		/* Empty for Day 2. */
	}
	
	public void integrate(double dt) {
		this.vel = this.vel.add(this.force.scale(dt / mass));
		this.pos = this.pos.add(this.vel.scale(dt));
	}
	
	public Vector getPos() {
		return this.pos;
	}
	
	public Vector getVel() {
		return this.vel;
	}
	
	public double getMass() {
		return this.mass;
	}
	
	public void setPos(Vector pos) {
		this.pos = pos;
	}
	
	public void setVel(Vector vel) {
		this.vel = vel;
	}
	
	public void setMass(double mass) {
		this.mass = mass;
	}
	public void clearNetForce() {
		this.force = new Vector(0, 0, 0);
	}
	
	public void applyForce(Vector force) {
		this.force = this.force.add(force);
	}
	
}
