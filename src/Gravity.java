
public class Gravity extends Force {

	Vector g;
	
	public Gravity() {
		super();
		
		g = new Vector(0, 0, -9.8);
	}
	
	public void integrate(double dt) {
		/* Does nothing, gravity is a constant :D */
	}
	
	public void applyForce() {
		for (int i = 0; i < this.objects.size(); i++) {
			PhysicsObject object = this.objects.get(i);
			object.applyForce(this.g.scale(object.getMass()));
		}
	}
	
}
